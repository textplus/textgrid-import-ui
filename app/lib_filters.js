function form_entry(entry){
    let html = ""
    let type_or_tag = entry[
        "type"
    ]  // input, textarea, checkbox, select, hidden, number, date, email, tel, $${HTML_tag}
    let name = entry["name"] ||  ""
    let label = entry["label"] ||  ""
    let after_label = entry["after_label"] ||  ""
    let value = entry["value"]
    let extra_html = entry["extra_html"] ||  ""
    let attrs = " ".join([`${k}="${v}"` for k, v in entry["attrs"] ||  {}).items()]
    let class_hidden = ""
    let br = "<br />"
    if (type_or_tag == "hidden"){
        class_hidden = " hidden"
        html = (
            `<input type="hidden" id="${name}" name="${name}" ${attrs} value="${value}" />`
        )
    } else if (type_or_tag == "checkbox"){
        checked = 'checked="checked"' if value else ""
        html = `<span class="label-etc"><input type="checkbox" id="${name}" name="${name}" ${checked} ${attrs}/>
<label for="${name}">${label}</label>${after_label}</span>`
    } else if (type_or_tag == "select"){
        options = [`<option value="${item}">${item}</option>` for item in value]
        html = `<span class="label-etc"><label for="${name}">${label}</label>${after_label}</span> <select 
id="${name}" name="${name}" ${attrs}>${options}</select>`
    } else if (type_or_tag == "input"){
        html = `<span class="label-etc"><label for="${name}">${label}</label>${after_label}</span><br /><input 
type="text" id="${name}" name="${name}" ${attrs} value="${value}" />`
    } else if (type_or_tag in ("number", "date", "email", "tel")){
        html = `<span class="label-etc"><label for="${name}">${label}</label>${after_label}</span><br /><input 
type="${type_or_tag}" id="${name}" name="${name}" ${attrs} value="${value}" />`
    } else if (type_or_tag == "textarea"){
        html = `<span class="label-etc"><label for="${name}">${label}</label>${after_label}</span><br /><textarea 
id="${name}" name="${name}" ${attrs}>${value}</textarea>`
    } else if (type_or_tag == "html"){
        html = value        
    } else:
        html = `<${type_or_tag} ${attrs}>${value}</${type_or_tag}>`
        br = ""
    return `<div class="form-entry${class_hidden}">${html + extra_html + br}</div>`
}