"Package initialization code"

import os
import shutil
import sys
import textgrid_import_ui as tgui

__version__ = "0.9.8"

def init_project():
    destdir = None
    if len(sys.argv) > 1:
        destdir = sys.argv[1]
    if not destdir:
        destdir = input("Path for new project:\n")
    destdir = os.path.abspath(destdir)
    if os.path.exists(destdir):
        print(f"{destdir} already exists.")
        return destdir
    try:
        codedir = os.path.dirname(tgui.__file__)
        shutil.copytree(codedir, destdir)
        print(f"Initialized project here: {destdir}")
        return destdir
    except Exception as e:
        print(f"Error creating {destdir}.\n{str(e)}")