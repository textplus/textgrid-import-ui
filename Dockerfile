# Using code from https://github.com/jupyter/docker-stacks/tree/main/images

# Ubuntu 22.04 (jammy)
# https://hub.docker.com/_/ubuntu/tags?page=1&name=jammy
ARG ROOT_CONTAINER=ubuntu:22.04

FROM $ROOT_CONTAINER

ARG NB_USER="jovyan"
ARG NB_UID="1000"
ARG NB_GID="100"

# Fix: https://github.com/hadolint/hadolint/wiki/DL4006
# Fix: https://github.com/koalaman/shellcheck/wiki/SC3014
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

USER root

# Install all OS dependencies for the Server that starts
# but lacks all features (e.g., download as all possible file formats)
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update --yes && \
    # - `apt-get upgrade` is run to patch known vulnerabilities in system packages
    #   as the Ubuntu base image is rebuilt too seldom sometimes (less than once a month)
    apt-get upgrade --yes && \
    apt-get install --yes --no-install-recommends \
    # - bzip2 is necessary to extract the micromamba executable.
    bzip2 \
    ca-certificates \
    locales \
    sudo \
    # - `tini` is installed as a helpful container entrypoint,
    #   that reaps zombie processes and such of the actual executable we want to start
    #   See https://github.com/krallin/tini#why-tini for details
    tini \
    wget && \
    apt-get clean && rm -rf /var/lib/apt/lists/* && \
    echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    echo "C.UTF-8 UTF-8" >> /etc/locale.gen && \
    locale-gen


# Enable prompt color in the skeleton .bashrc before creating the default NB_USER
# hadolint ignore=SC2016
RUN sed -i 's/^#force_color_prompt=yes/force_color_prompt=yes/' /etc/skel/.bashrc

# Create NB_USER with name jovyan user with UID=1000 and in the 'users' group
# and make sure these dirs are writable by the `users` group.
RUN echo "auth requisite pam_deny.so" >> /etc/pam.d/su && \
    sed -i.bak -e 's/^%admin/#%admin/' /etc/sudoers && \
    sed -i.bak -e 's/^%sudo/#%sudo/' /etc/sudoers && \
    useradd --no-log-init --create-home --shell /bin/bash --uid "${NB_UID}" --no-user-group "${NB_USER}"
    #  && \
    # mkdir -p "${CONDA_DIR}" && \
    # chown "${NB_USER}:${NB_GID}" "${CONDA_DIR}" && \
    # chmod g+w /etc/passwd && \
    # fix-permissions "${CONDA_DIR}" && \
    # fix-permissions "/home/${NB_USER}"

USER ${NB_UID}

# Pin the Python version here, or set it to "default"
ARG PYTHON_VERSION=3.11



USER root

# RUN apt-get update && \
#   apt install -y build-essential
RUN apt-get update \
    && apt-get -y --no-install-recommends install build-essential gcc python3-dev python3-venv \
      rsync git nano vim joe mc man less zip unzip  p7zip-full jq \
    && apt-get clean && apt-get autoclean \
    # && chsh -s /bin/bash jovyan \
    && mkdir /var/textplus \
    && chown jovyan /var/textplus

COPY scripts/* /var/textplus/
RUN chown -R jovyan /var/textplus && chmod +x /var/textplus/*.sh

USER jovyan
WORKDIR /home/jovyan

RUN env_path="/var/textplus/env_tgimport" \
&& targetdir=/var/textplus \
&& env_name=env_tgimport \
&& /var/textplus/setup-tgimport-ui.sh  ${env_path} ${env_name} ${targetdir} "v0.9.8"

EXPOSE 8888
# CMD ["/var/textplus/start-server.sh"]

# Configure container entrypoint
ENTRYPOINT ["tini", "-g", "--", "/var/textplus/start-server.sh"]
