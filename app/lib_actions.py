import json
import sys
import os
import re
import jupyter
import yaml
import lib_util
import lib_actions_custom
from xml.sax.saxutils import escape


# logging.basicConfig(level=logging.DEBUG)
tmpdir = "./app/tmp"
logfile = f"{tmpdir}/log.txt"
logfile_abs = os.path.abspath(logfile)
if os.path.exists(logfile):
    os.remove(logfile)
if not os.path.exists(tmpdir):
    os.makedirs(tmpdir)
app_state_file = "./app/tmp/state.json"
# forms_config_file = "./app/forms_config.json"
# forms_config = lib_util.load_json(forms_config_file)
# envpath = "/var/textplus/env_tgimport"
envpath = jupyter.__file__.rsplit(f"{os.sep}lib{os.sep}", 1)[0]


def resolve_path(path, app_env):
    # if path.split("/")[0] in (".", "..", ""):
    #     return path
    # else:
    #     return f"{root_dir}/{path}"
    return os.path.abspath(path.format(**app_env))


def unflatten(val: dict) -> dict:
    try:
        # ensure only values serializable as json are accepted
        val = json.loads(json.dumps(val))
        if not isinstance(val, dict):
            return val
        temp_data = {"toplevel": {}}
        for path, v in val.items():
            path_parts = ["toplevel"] + path.split(".")
            for i, part in enumerate(path_parts):
                container_is_list = False
                if re.match(r"\d+$", part):
                    part = int(part)
                    container_is_list = True
                container_path = ".".join(path_parts[:i])
                if container_path not in temp_data:
                    temp_data[container_path] = {}
                    if container_is_list:
                        temp_data[container_path] = []
                if container_is_list:
                    temp_data[container_path].append(v)
                else:
                    temp_data[container_path][part] = v
        for k, v in sorted(temp_data.items(), reverse=True):
            path_parts = k.split(".")
            for i, part in enumerate(reversed(path_parts)):
                if i < len(path_parts) - 1:
                    path_container = ".".join(path_parts[: len(path_parts) - i - 1])
                    temp_data[path_container][part] = v
        return temp_data["toplevel"]
    except Exception as e:
        sys.stderr.write(str(e))
        return val


def create_form_entries(query_info):
    form = (
        []
    )  # {"type": "hidden", "name": "Name", "value": query_info["add_action__action_name"]}
    output_options = [
        {
            "type": "checkbox",
            "name": "show_output",
            "label": "Show output",
            "value": True,
        },
        {
            "type": "hidden",
            "name": "output_to_file",
            "label": "Save output to file:",
            "value": "{root_dir}/projects/nb-web-ui/app/tmp/out.txt",
            "attrs": {
                "class": "action onclick",
                "data-action": "becomeTarget_addField",
            },
        },
    ]
    form_entry = {"type": "input"}
    # form entries can contain: type, name, label, after_label, value, extra_html, attrs
    if query_info["type"] == "Script":
        form_entry["name"] = "script"
        form_entry["label"] = "Script:"
        form_entry["value"] = query_info["script"]
        form_entry["attrs"] = {"class": "action onclick", "data-action": "becomeTarget"}
    elif query_info["type"] == "CLI command":
        form_entry["name"] = "command"
        form_entry["label"] = "Command:"
        form_entry["value"] = query_info["command"]
    elif query_info["type"] == "Action function":
        form_entry["name"] = "function"
        form_entry["label"] = "Action function:"
        form_entry["value"] = query_info["function"]
        # form_entry["type"] = "hidden"
    if query_info.get("options_json", "").strip():
        try:
            form += json.loads(query_info["options_json"])
        except Exception as e:
            print("Error parsing JSON", str(e))
    else:
        if "option_yaml" in query_info:
            for item in query_info["option_yaml"]:
                try:
                    form_entry = yaml.safe_load(item)
                    if form_entry:
                        form.append(form_entry)
                # except yaml.ParserError:
                except Exception as e:
                    print("Error parsing YAML", str(e))
        if "options" in query_info:
            for option in query_info["options"]:
                if option.strip():
                    form_entry = {}
                    name, value = (
                        option.split("=", 1) if "=" in option else (option, "")
                    )
                    form_entry["name"] = name
                    form_entry["label"] = name
                    form_entry["value"] = value
                    form_entry["attrs"] = {
                        "class": "action onclick",
                        # "data-action": "becomeTarget",
                        "data-action": "formEntry_addWidgets",
                        "data-widgets": "More values",  # Use selected path;Go to;
                    }
                    form_entry["type"] = form_entry.get("type", "input")
                    form.append(form_entry)
    if query_info["type"] == "CLI command":
        form.append(output_options)
    form = form + [
        {
            "type": "button",
            "value": "Run",
            "attrs": {
                "class": "action onclick",
                "data-action": "submitForm",
                "data-action-server": "run_action",
                "data-view": "view1",
            },
        },
    ]
    return form


def action__startview(query_info={}, app_env={}):
    forms_config = app_env["forms_config"]
    template = app_env["jinja_env"].get_template("fs.html")
    secrets = lib_util.load_json(app_env["secrets_file"])
    lib_util.update_values_in_json_file(app_state_file, secrets)

    html = template.render(
        view=query_info.get("view", "view1"),
        root_url=app_env["root_url"],
        notebookdir_rel=app_env["notebookdir_rel"],
        api_token=app_env["api_token"],
        form=forms_config["Start"],
        form_path=["Load workflow", "TextGrid Import", "TextGrid Import | Store session ID"],
    )
    return html


def action__add_action(query_info={}, app_env={}):
    query_info = unflatten(query_info)
    forms_config = app_env["forms_config"]
    forms_config_file = app_env["forms_config_file"]
    template = app_env["jinja_env"].get_template("fs.html")
    secrets = lib_util.load_json(app_env["secrets_file"])
    lib_util.update_values_in_json_file(app_state_file, secrets)
    action_name = query_info["add_action__action_name"]
    all_action_names = forms_config["Load action"][0]["value"]
    c = 2
    new_action_name = action_name
    while new_action_name in all_action_names:
        new_action_name = f"{action_name} ({c})"
        c += 1
    form = create_form_entries(query_info)

    forms_config["Load action"][0]["value"].append(new_action_name)
    # forms_config["Edit action"] = json.loads(json.dumps(forms_config["Load action"]))
    forms_config["Edit action"][0]["value"] = forms_config["Load action"][0]["value"]

    forms_config[new_action_name] = form
    lib_util.tofile(forms_config_file, forms_config, asjson=True)

    html = template.render(
        view=query_info.get("view", "view1"),
        root_url=app_env["root_url"],
        notebookdir_rel=app_env["notebookdir_rel"],
        api_token=app_env["api_token"],
        form=forms_config["Start"],
        form_path=["Load action", new_action_name],
    )
    return html


def action__edit_action(query_info={}, app_env={}):
    forms_config = app_env["forms_config"]
    forms_config_file = app_env["forms_config_file"]
    edit_action__selection = query_info["edit_action__selection"]
    template = app_env["jinja_env"].get_template("fs.html")
    action_name = query_info["selection_level3"]
    if edit_action__selection == "Remove action":
        actions = [
            item
            for item in forms_config["Load action"][0]["value"]
            if item != action_name
        ]
        forms_config["Load action"][0]["value"] = actions
        forms_config["Edit action"][0]["value"] = actions
        if action_name in forms_config:
            del forms_config[action_name]
    elif edit_action__selection == "Edit action as JSON":
        try:
            action_cfg = json.loads(
                query_info.get("options_json", forms_config[action_name])
            )
            forms_config[action_name] = action_cfg
        except Exception as e:
            print(str(e))
    lib_util.tofile(forms_config_file, forms_config, asjson=True)
    html = template.render(
        view=query_info.get("view", "view1"),
        root_url=app_env["root_url"],
        notebookdir_rel=app_env["notebookdir_rel"],
        api_token=app_env["api_token"],
        form=forms_config["Start"],
        form_path=["Edit action"],
    )
    return html


def action__remove_action(query_info={}, app_env={}):
    forms_config = app_env["forms_config"]
    forms_config_file = app_env["forms_config_file"]
    template = app_env["jinja_env"].get_template("fs.html")
    action_name = query_info["selection_level3"]
    actions = [
        item for item in forms_config["Load action"][0]["value"] if item != action_name
    ]
    forms_config["Load action"][0]["value"] = actions
    del forms_config[action_name]
    lib_util.tofile(forms_config_file, forms_config, asjson=True)
    html = template.render(
        view=query_info.get("view", "view1"),
        root_url=app_env["root_url"],
        notebookdir_rel=app_env["notebookdir_rel"],
        api_token=app_env["api_token"],
        form=forms_config["Start"],
        form_path=["Load action"],
    )
    return html


def action__run_action(query_info={}, app_env={}):
    query_info = unflatten(query_info)
    forms_config = app_env["forms_config"]
    template = app_env["jinja_env"].get_template("fs.html")
    action_output = ""
    if "command" in query_info:
        cmd = query_info["command"].format(**query_info).format(**app_env)
        out, err = lib_util.run_system_command(cmd, catch_errors=True)
        if query_info["output_to_file"]:
            output_to_file = query_info["output_to_file"].format(**app_env)
            try:
                if err:
                    out = f"{out}\n{err}"
                lib_util.tofile(output_to_file, out, verbose=False)
            except Exception as e:
                sys.stderr.write(str(e))
        if query_info["show_output"]:
            action_output = escape(out)
        html = template.render(
            view=query_info.get("view", "view1"),
            root_url=app_env["root_url"],
            notebookdir_rel=app_env["notebookdir_rel"],
            api_token=app_env["api_token"],
            form=forms_config["Start"],
            form_path=[query_info["selection_level1"], query_info["selection_level3"]],
            action_output=action_output,
        )
        return html
    elif "function" in query_info:
        action = getattr(lib_actions_custom, query_info["function"])
        return action(query_info, app_env)


def action__add_workflow(query_info={}, app_env={}):
    query_info = unflatten(query_info)
    forms_config = app_env["forms_config"]
    forms_config_file = app_env["forms_config_file"]
    template = app_env["jinja_env"].get_template("fs.html")
    secrets = lib_util.load_json(app_env["secrets_file"])
    lib_util.update_values_in_json_file(app_state_file, secrets)
    # print(str(query_info))
    workflow_name = query_info["add_workflow__workflow_name"]
    form = [
        {
            "type": "select",
            "name": "selection_level3",
            "label": "Step",
            "value": [
                "----",
            ],
            "attrs": {
                "class": "action onchange",
                "data-action-onchange": "showForm",
                "data-dest": ".form-load_workflow",
            },
        },
        {"type": "html", "value": '<div class="form-load_workflow"></div>'},
    ]
    forms_config["Load workflow"][0]["value"].append(workflow_name)
    forms_config["Edit workflow"][0]["value"] = forms_config["Load workflow"][0][
        "value"
    ]
    forms_config[query_info["add_workflow__workflow_name"]] = form
    lib_util.tofile(forms_config_file, forms_config, asjson=True)
    html = template.render(
        view=query_info.get("view", "view1"),
        root_url=app_env["root_url"],
        notebookdir_rel=app_env["notebookdir_rel"],
        api_token=app_env["api_token"],
        form=forms_config["Start"],
        form_path=["Edit workflow", workflow_name],
    )
    return html


def action__edit_workflow(query_info={}, app_env={}):
    query_info = unflatten(query_info)
    forms_config = app_env["forms_config"]
    forms_config_file = app_env["forms_config_file"]
    template = app_env["jinja_env"].get_template("fs.html")
    workflow_name = query_info["selection_level2"]
    formpath_after = ["Edit workflow", workflow_name]
    if not re.match(r"^\-\-.*\-\-$", workflow_name):
        if query_info["edit_workflow__selection"].endswith("Load action") or query_info[
            "edit_workflow__selection"
        ].endswith("New action"):
            formpath_after = ["Load workflow", workflow_name]
            step_options = {
                k: query_info[k]
                for k in query_info.keys()
                if k
                not in [
                    "action",
                    "selection_level1",
                    "selection_level2",
                    # "edit_workflow__selection",
                    "view",
                ]
            }
            # print("step_options", step_options)
            action_name = query_info.get(
                "add_action__action_name", query_info.get("selection_level3", "---")
            )
            all_step_names = forms_config[workflow_name][0]["value"]
            step_name_qualified = f"{workflow_name} | {action_name}"
            c = 2
            step_name = step_name_qualified
            while step_name in all_step_names:
                step_name = f"{step_name_qualified} ({c})"
                c += 1

            step_form_config = []
            if query_info["edit_workflow__selection"].endswith("Load action"):
                step_form_config = unflatten(forms_config.get(action_name, []))
                # print("step_form_config", step_form_config)

                # override the action settings with values in the "Load action" form:
                for i, entry in enumerate(step_form_config):
                    if "name" in entry and entry["name"] in step_options:
                        step_form_config[i]["value"] = step_options[entry["name"]]
            elif query_info["edit_workflow__selection"].endswith("New action"):
                step_form_config = create_form_entries(query_info)
            # step_form_config.insert(0, {"type": "h4", "value": step_name})

            insert_after_step = query_info.get(
                "selection_level3_0", query_info.get("selection_level3", None)
            )
            if insert_after_step and insert_after_step in all_step_names:
                pos = all_step_names.index(insert_after_step) + 1
            else:
                pos = len(all_step_names)
            forms_config[workflow_name][0]["value"].insert(pos, step_name)
            forms_config[step_name] = step_form_config
            lib_util.tofile(forms_config_file, forms_config, asjson=True)
        elif query_info["edit_workflow__selection"].endswith("Remove step"):
            if "selection_level3" in query_info:
                selected_workflow_step = query_info["selection_level3"]
                if not re.match(r"^\-\-.*\-\-$", selected_workflow_step):
                    forms_config[workflow_name][0]["value"] = [
                        item
                        for item in forms_config[workflow_name][0]["value"]
                        if item != selected_workflow_step
                    ]
                    if selected_workflow_step in forms_config:
                        del forms_config[selected_workflow_step]
                    lib_util.tofile(forms_config_file, forms_config, asjson=True)
        elif query_info["edit_workflow__selection"].endswith("Remove workflow"):
            formpath_after = ["Edit workflow"]
            if workflow_name in forms_config:
                del forms_config[workflow_name]
            forms_config["Load workflow"][0]["value"] = [
                item
                for item in forms_config["Load workflow"][0]["value"]
                if item != workflow_name
            ]
            forms_config["Edit workflow"][0]["value"] = forms_config["Load workflow"][
                0
            ]["value"]
            lib_util.tofile(forms_config_file, forms_config, asjson=True)

    html = template.render(
        view=query_info.get("view", "view1"),
        root_url=app_env["root_url"],
        notebookdir_rel=app_env["notebookdir_rel"],
        api_token=app_env["api_token"],
        form=forms_config["Start"],
        form_path=formpath_after,
    )
    return html
