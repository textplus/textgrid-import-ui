#! /bin/bash
targetdir=/var/textplus
env_name=env_tgimport
env_path=/var/textplus/env_tgimport
repo_name=textgrid-import-ui
repo_url=https://oauth:UcX7kPHBvuG1j1z4WxTb@gitlab.gwdg.de/textplus/textplus-io/textgrid-import-ui.git
if test -n "$1"; then
    env_path=$1 
fi
if test -n "$2"; then
    env_name=$2
fi
if test -n "$3"; then
    targetdir=$3
fi
display_name="Python 3 | Text+ (${env_name})"

if [ ! -d ${env_path} ]; then
    /usr/bin/python3 -m venv ${env_path}
fi
${env_path}/bin/pip install jupyterhub jupyterlab ipywidgets ipykernel

cd ${targetdir}
if [ -d "${targetdir}/${repo_name}" ]; then  
    cd ${repo_name}
else
    git clone ${repo_url} \
    && cd ${repo_name}
    if test -n "$4"; then
        tag=$4 
    else
        tag=$(git describe --abbrev=0)
    fi
    git checkout ${tag}
fi
chmod +x scripts/*.sh && chmod +x app/*.sh && ${env_path}/bin/pip install -r "requirements.txt"

./app/01-get-client-libs.sh

# Remove kernel if it already exists, then link it to jupyter:
${env_path}/bin/jupyter kernelspec uninstall -f "${env_name}"
"${env_path}/bin/python" -m ipykernel install --user --name="${env_name}" --display-name "${display_name}" 
