window.fs = async (viewName) => {
    let viewSelector = (viewName) ? `.${viewName} ` : "";
    let otherView = (viewName == "view1") ? "view2" : "view1";
    let rootUrl = window.rootUrl;
    let notebookdir_rel = window.notebookdir_rel;
    let api_token = window.api_token;
    let formsConfig = {};
    $(viewSelector + " *").off("click mouseover mouseenter mouseout mouseleave keydown keyup keypress change");

    let clientActions = {};

    const sendQueryThenPushButton = (queryObj, btnLabel, tmpFile = "app/tmp/tmp_req.json") => {
        writeToFile(`${rootUrl}/api/contents/${notebookdir_rel}/${tmpFile}`
            + `?token=${api_token}&_xsrf=${_xsrf}`,
            JSON.stringify(queryObj, null, 2),
            (resp) => {
                console.log(resp);
                btn$ = $(`button.jupyter-button:contains(${btnLabel})`).eq(0);
                btn$.click();
            }
        );

    };

    const getRelativePath = (path, relativeTo) => {
        let parts = path.split("/");
        let parts_relTo = relativeTo.split("/");

        let commonParts = [];
        let otherParts = path.split("/");
        let otherParts_relTo = relativeTo.split("/");
        for (let i = 0; i < parts.length; i += 1) {
            if (i + 1 > parts_relTo.length) {
                break;
            }
            if (parts[i] == parts_relTo[i]) {
                commonParts.push(otherParts.shift());
                otherParts_relTo.shift();
            } else {
                break;
            }
        }
        let result = [];
        if (commonParts.length) {
            for (let item of otherParts_relTo) {
                result.push("..");
            }
            for (let item of otherParts) {
                result.push(item);
            }
            return result.join("/");
        } else {
            return path;
        }
    }

    const resolvePath = (base, relative) => {
        // adapted from https://stackoverflow.com/questions/14780350/convert-relative-path-to-absolute-using-javascript
        let stack = base.split("/"),
            parts = relative.split("/");
        stack.pop(); // remove current file name (or empty string)
        // (omit if "base" is the current folder without trailing slash)
        for (let i = 0; i < parts.length; i++) {
            if (parts[i] == ".")
                continue;
            if (parts[i] == "..")
                stack.pop();
            else
                stack.push(parts[i]);
        }
        return stack.join("/");
    }

    const toFormEntry = (entry) => {
        let html = ""
        let type_or_tag = entry[
            "type"
        ]  // input, textarea, checkbox, select, hidden, number, date, email, tel, $${HTML_tag}
        let name = entry["name"] || ""
        let label = entry["label"] || ""
        let after_label = entry["after_label"] || ""
        let value = entry["value"]
        let extra_html = entry["extra_html"] || ""
        let attrs = [];
        for (const [k, v] of Object.entries(entry["attrs"] || {})) {
            attrs.push(`${k}="${v}"`)
        }
        attrs = attrs.join(" ")
        // let attrs = " ".join([`${k}="${v}"` for k, v in entry["attrs"] ||  {}).items()]
        let class_hidden = ""
        let br = "<br />"
        if (type_or_tag == "hidden") {
            class_hidden = " hidden"
            html = (
                `<input type="hidden" id="${name}" name="${name}" ${attrs} value="${value}" />`
            )
        } else if (type_or_tag == "checkbox") {
            // checked = 'checked="checked"' if value else ""
            checked = (value) ? 'checked="checked"' : ""
            html = `<span class="label-etc"><input type="checkbox" id="${name}" name="${name}" ${checked} ${attrs}/>
    <label for="${name}">${label}</label>${after_label}</span>`
        } else if (type_or_tag == "select") {
            // options = [`<option value="${item}">${item}</option>` for item in value]
            options = value.map(item => `<option value="${item}">${item}</option>`).join(" ")
            html = `<span class="label-etc"><label for="${name}">${label}</label>${after_label}</span> <select 
    id="${name}" name="${name}" ${attrs}>${options}</select>`
        } else if (type_or_tag == "input") {
            html = `<span class="label-etc"><label for="${name}">${label}</label>${after_label}</span><br /><input 
    type="text" id="${name}" name="${name}" ${attrs} value="${value}" />`
        } else if (["number", "date", "email", "tel"].includes(type_or_tag)) {
            html = `<span class="label-etc"><label for="${name}">${label}</label>${after_label}</span><br /><input 
    type="${type_or_tag}" id="${name}" name="${name}" ${attrs} value="${value}" />`
        } else if (type_or_tag == "textarea") {
            html = `<span class="label-etc"><label for="${name}">${label}</label>${after_label}</span><br /><textarea 
    id="${name}" name="${name}" ${attrs}>${value}</textarea>`
        } else if (type_or_tag == "html") {
            html = value
        } else {
            html = `<${type_or_tag} ${attrs}>${value}</${type_or_tag}>`
            br = ""
        }
        return `<div class="form-entry${class_hidden}">${html + extra_html + br}</div>`
    }

    const toForm = (formData, destSelector) => {
        let formAsHtml = "";
        for (const entry of formData) {
            formAsHtml += toFormEntry(entry);
        }
        formAsHtml = `<form>${formAsHtml}</form>`;

        if (destSelector) {
            $(destSelector).append(formAsHtml);
        }
        return formAsHtml;
    };

    const activateHandlers = () => {
        $(viewSelector + " *").off("click mouseover mouseenter mouseout mouseleave keydown keyup keypress change");

        $(viewSelector + " .action.onclick").on("click", (evt) => {
            let actionElem$ = $(evt.target).closest(".action");
            let clientAction = actionElem$.attr("data-action");
            // let actions = actionElem$.attr("data-action").split(",");
            // for (let item of actions) {
            //     let clientAction = item.trim();
            //     clientActions[clientAction](evt);
            //   }
            clientActions[clientAction](evt);
        });

        $(viewSelector + " .action.ondblclick").on("dblclick", (evt) => {
            let actionElem$ = $(evt.target).closest(".action.ondblclick");
            let clientAction = actionElem$.attr("data-action-ondblclick");
            clientActions[clientAction](evt);
        });

        $(viewSelector + " .action.onchange").on("change", (evt) => {
            let actionElem$ = $(evt.target).closest(".action.onchange");
            let clientAction = actionElem$.attr("data-action-onchange");
            clientActions[clientAction](evt);
        });
    };

    const showContentsOf = async (url, targetSelector, selectItem = null) => {
        // use date as URL param to avoid receiving a cached response
        const response = await fetch(`${url}?token=${api_token}&_xsrf=${_xsrf}&${Date.now()}`);
        const data = await response.json();
        let items = data.content;
        items.sort((a, b) => a.name - b.name);
        items.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
        let relPath = url.replace(`${rootUrl}/api/contents/`, "");
        let relPathParts = relPath.split("/");
        let rootDirBtn = `<span class="action onclick fs-dir" data-action="showContents" data-path=""><img src="${rootUrl}/files/${notebookdir_rel}/app/web/img/folder.svg" alt="Root folder" width="16" height="16"/> </span>`;
        let breadCrumb = [rootDirBtn];
        for (let i = 0; i < relPathParts.length; ++i) {
            let part = relPathParts[i];
            let path = relPathParts.slice(0, i + 1).join("/");
            breadCrumb.push(`<span class="action onclick fs-dir" data-action="showContents" data-path="${path}">${part}</span>`);
        }
        let dirs = [`<div class="breadcrumb">${breadCrumb.join("/")}</div>`];
        let files = [];
        for (const item of items) {
            let attrsToShow = ["name", "path", "created", "last_modified", "size"];
            let attrs = attrsToShow.map((k) => `data-${k}="${item[k]}"`).join(" ");
            let title = attrsToShow.map((k) => `${k}: ${item[k]}`).join("&#10;");
            let itemAsHtml;
            let selected = (selectItem && item.name == selectItem) ? "fs-selected " : "";
            if (item.type === "directory") {
                itemAsHtml = `<div class="${selected}fs-dir fs-item action onclick ondblclick" data-action="selectFSItem" data-action-ondblclick="showContents" title="${title}" ${attrs}>${item.name}</div>`;
                dirs.push(itemAsHtml);
            } else {
                itemAsHtml = `<div class="${selected}fs-file fs-item action onclick ondblclick" data-action="selectFSItem"  title="${title}" ${attrs}>${item.name}</div>`
                files.push(itemAsHtml);
            }
        }
        $(targetSelector).html(dirs.join("\n") + files.join("\n"));
        activateHandlers();
    };

    loadFormsConfig = async () => {
        const response = await fetch(`${rootUrl}/api/contents/${notebookdir_rel}/app/forms_config.json?token=${api_token}&_xsrf=${_xsrf}&${Date.now()}`);
        let resp = await response.json();
        formsConfig = JSON.parse(resp.content);
        return formsConfig;
    };

    clientActions.showContents = (evt) => {
        let actionElem$ = $(evt.target).closest(".action");
        let url = `${rootUrl}/api/contents/${actionElem$.attr("data-path")}`;
        showContentsOf(url, viewSelector + " .fs-contents");
    };

    clientActions.createLinkToViewFile = async (evt) => {
        let selectedItem$ = $(".fs-selected");
        if (selectedItem$.length) {
            let path = selectedItem$.attr("data-path");
            let internalUrl = `${rootUrl}/files/${path}`;
            let fileLinkWrapper$ = $(viewSelector + " .fileLinkWrapper");
            fileLinkWrapper$.html(`<a class="viewFile" href="${internalUrl}"
            rel="noopener" target="_blank"  data-commandLinker-command="rendermime:handle-local-link"
            data-commandlinker-args="{&quot;path&quot;:&quot;${path}&quot;,&quot;id&quot;:&quot;&quot;}"
            >Open</a>`);
        }
    };

    clientActions.showInFileBrowser = (evt) => {
        let value;
        let formEntry$ = $(evt.target).closest(".form-entry");
        if (formEntry$.length) {
            value = formEntry$.find(":input").val();
        } else {
            value = $(evt.target).closest(".fs-path").text().trim();
        }
        if (value) {
            let path = (value.indexOf("../") == 0 || value.indexOf("./") == 0) ? `${notebookdir_rel}/${value}` : value;
            path = path.replace(/^\{root_dir\}\//, "");
            path = resolvePath("", path);
            let itemName = path.split("/").pop();
            parentDir = resolvePath("", `${path}/..`);
            showContentsOf(
                `${rootUrl}/api/contents/${parentDir}`,
                `${viewSelector} .fs-contents`,
                itemName
            );
        }
    };

    clientActions.goToStartView = (evt) => {
        sendQueryThenPushButton({
            "action": "startview",
            "update_app_state": "./app/tmp/state.json",
        }, "view1");
    };

    clientActions.goToStartdir = (evt) => {
        showContentsOf(
            `${rootUrl}/api/contents/${notebookdir_rel}`,
            `${viewSelector} .fs-contents`,
            null
        );
    };

    clientActions.clearTempWidgets = (evt) => {
        $(viewSelector + " .tmpwidget").remove();
        $(viewSelector + " .fs-target").removeClass("fs-target");
    };

    clientActions.formEntry_addWidgets = (evt, widgets) => {
        let actionElem$ = $(evt.target).closest(".action");
        widgets = widgets || (actionElem$.attr("data-widgets") || "").split(";");

        $(viewSelector + " .tmpwidget").remove();
        if (actionElem$.hasClass("fs-target")) {
            actionElem$.removeClass("fs-target");
        } else {
            let formEntry$ = actionElem$.closest(".form-entry");
            let tempWidgets = "";
            if (widgets.includes("Use selected path")) {
                tempWidgets += `<span title="Use path selected in file browser" 
class="tmpwidget use-fs-selected action onclick text-primary badge badge-pill" 
data-action="updateTarget">Use selected path</span>`
            }
            if (widgets.includes("Go to")) {
                tempWidgets += `<span title="Show path in file browser" 
class="tmpwidget action onclick text-primary badge badge-pill" 
data-action="showInFileBrowser">Go to</span>`
            }
            if (widgets.includes("Linebreak")) {
                tempWidgets += `<span title="Add linebreak" 
class="tmpwidget action onclick text-primary badge badge-pill" 
data-action="addLinebreak">Linebreak</span>`
            }
            if (widgets.includes("Current config")) {
                tempWidgets += `<span title="Get current config as JSON" 
class="tmpwidget action onclick text-primary badge badge-pill" 
data-action="getActionConfig">Current config</span>`
            }
            if (widgets.includes("More values")) {
                let nextFormEntry$ = formEntry$.next(".form-entry");
                if (!nextFormEntry$.length || !nextFormEntry$.hasClass("form-entry-extra")) {
                    tempWidgets += `<span title="Add extra field" 
class="tmpwidget action onclick text-primary badge badge-pill" 
data-action="addField">More values</span>`;
                }
            }
            if (formEntry$.hasClass("form-entry-extra")) {
                formEntry$.find("br").eq(-1).before(`<span title="Remove field" 
class="tmpwidget action onclick text-primary badge badge-pill" 
data-action="removeField">Discard</span>`);
            }
            actionElem$.after(tempWidgets);
            $(viewSelector + " .fs-target").removeClass("fs-target");
            actionElem$.addClass("fs-target");
            activateHandlers();
        }
    };

    clientActions.becomeTarget = (evt, addField) => {
        clientActions.formEntry_addWidgets(evt, ["Use selected path", "Go to"]);
    };

    clientActions.becomeTarget_addField = (evt) => {
        clientActions.formEntry_addWidgets(evt, ["Use selected path", "Go to", "More values"]);
    };

    clientActions.updateTarget = (evt) => {
        let path = $(viewSelector + " .fs-selected").attr("data-path");
        if (path) {
            $(viewSelector + " .fs-target").val("{root_dir}/" + path);
            // to do: use double braces

            // let url = `${rootUrl}/api/contents/${path}`;
            // showContentsOf(url, viewSelector + " .info-bottom");
        }
    };


    clientActions.addField = (evt) => {
        // Clone the existing field. Before cloning, ensure the name of the field
        // ends with a dot and a number. To obtain the name of the new field,
        // increment the number part of the original entry name. 
        let formEntry$ = $(evt.target).closest(".form-entry");
        clientActions.clearTempWidgets();
        let contentElem$ = formEntry$.find(":input");
        let label$ = formEntry$.find("label");
        let entryName = contentElem$.attr("name");
        let oldNumberPart;
        if (!entryName.match(/\.\d+$/)) {
            entryName = `${entryName}.0`;
            contentElem$.attr("name", entryName).attr("id", entryName);
            label$.attr("for", entryName);
        }
        let entryNameParts = entryName.split(".");
        oldNumberPart = Number(entryNameParts.pop());
        let newName = `${entryNameParts.join(".")}.${oldNumberPart + 1}`;
        let newFormEntry$ = formEntry$.clone();
        let newContentElem$ = newFormEntry$.find(":input");
        let newLabel$ = newFormEntry$.find("label");
        newContentElem$.val("");
        newContentElem$.attr("name", newName).attr("id", newName);
        newLabel$.attr("for", newName);
        newFormEntry$.addClass("form-entry-extra");
        formEntry$.after(newFormEntry$);
        activateHandlers();
    };

    clientActions.removeField = (evt) => {
        $(evt.target).closest(".form-entry").remove();
    };

    clientActions.addLinebreak = (evt) => {
        let ta$ = $(evt.target).closest(".form-entry").find("textarea");
        let cursorPosition = ta$.prop("selectionStart");
        let text = ta$.val();
        ta$.val(text.substring(0, cursorPosition) + "\n" + text.substring(cursorPosition));
        ta$.focus();
    };

    clientActions.getActionConfig = (evt) => {
        let ta$ = $(evt.target).closest(".form-entry").find("textarea");
        let actionName = ta$.closest("form").find("select[name=selection_level3]").val();
        console.log(actionName, formsConfig[actionName]);
        let cfg = JSON.stringify(formsConfig[actionName], null, 4);
        ta$.val(cfg);
    };

    clientActions.selectFSItem = (evt) => {
        let actionElem$ = $(evt.target).closest(".action");
        $(viewSelector + " .fs-selected").removeClass("fs-selected");
        actionElem$.addClass("fs-selected");
        $(viewSelector + " .fileLinkWrapper").html("");
        if (actionElem$.hasClass("fs-file")) {
            clientActions.createLinkToViewFile(evt);
        }
    };

    clientActions.toggleFileBrowser = (evt) => {
        let fsb$ = $(".fs-browser");
        if (fsb$.hasClass("hidden")) {
            fsb$.removeClass("hidden");
        } else {
            fsb$.addClass("hidden");
        }
    };

    clientActions.submitForm = (evt) => {
        evt.preventDefault();
        let actionElem$ = $(evt.target).closest(".action");
        let targetView = actionElem$.attr("data-view");
        let form$ = actionElem$.closest("form");
        let formData = { "action": actionElem$.attr("data-action-server") };
        let problems = [];
        form$.find("input, textarea, select").each(function (i, elem) {
            let elem$ = $(elem);
            let name = elem$.attr("name");
            let value = elem$.val();
            if (elem$.attr("type") === "checkbox") {
                value = (elem$.is(':checked')) ? true : false;
            } else {
                entry$ = elem$.closest(".form-entry");
                if (elem$.is(".required")) {
                    if (!value.trim()) {
                        entry$.find("label").addClass("problem");
                        problems.push(entry$);
                    } else {
                        entry$.find("label").removeClass("problem");
                    }
                }
            }
            if (name in formData) {
                formData[name + "_0"] = formData[name];
            }
            formData[name] = value;
        });
        console.log(formData);
        if (!problems.length) {
            sendQueryThenPushButton(formData, targetView);
        }
    };

    clientActions.loadValuesFromFile = async (evt) => {
        // populates a form with values from a JSON file
        evt.preventDefault();
        let btn$ = $(evt.target);
        let file = btn$.attr("data-file");
        console.log(file);
        const response = await fetch(`${rootUrl}/api/contents/${notebookdir_rel}/${file}?token=${api_token}&_xsrf=${_xsrf}&${Date.now()}`);
        let info = {}
        if (response.status !== 404) {
            info = await response.json();
            info = JSON.parse(info.content);
            console.log(info, typeof info);
        } else { console.log("Not found:", file); }
        btn$.closest("form").find(":input").each((i, elem) => {
            if ($(elem).is("textarea, input[type='text']")) {
                let name, index;
                name = elem.getAttribute("name");
                if (name.match(/^\w+\.\d+$/)) {
                    parts = name.split(".");
                    name = parts[0];
                    index = parts[1];
                }
                console.log(name);
                if (name in info) {
                    console.log(name, index);
                    if (index !== undefined) {
                        $(elem).val(info[name][index]);
                    } else {
                        $(elem).val(info[name]);
                    }
                }
            }
        });
    };

    clientActions.showForm = (evt, elem) => {
        // when selecting a value, load content from formsConfig corresponding to that value
        // but not in edit mode (not when the top select element is "Edit ...")
        let target$ = (evt) ? $(evt.target) : $(elem);
        let val = target$.val();
        let valParts = val.split(" | ");
        let valNoPrefix = val.split(" | ").slice(-1)[0];
        if (target$.attr("name").indexOf("_level1") !== -1) {
            $(viewSelector + " .contents").attr("class", "contents " + val.replace(/[\s\|]/g, "_"));
        }
        if (target$.attr("name").indexOf("_level3") !== -1) {
            if ($(".contents.Edit_action, .contents.Edit_workflow").length) {
                return;
            }
        }
        let formsData = formsConfig[val] || formsConfig[valParts[1]] || formsConfig[valParts[0]] || [];
        // console.log(val, valNoPrefix, formsData);
        let html = toForm(formsData);
        let destSelector = target$.attr("data-dest");
        $(destSelector).html(html);
        activateHandlers();
    };

    const startUrl = rootUrl + `/api/contents/${notebookdir_rel}`;
    showContentsOf(startUrl, viewSelector + " .fs-contents");

    formsConfig = await loadFormsConfig();

    console.log("formsConfig", formsConfig);
    $(function () {
        $(".goto-formpath span").each(function (i, elem) {
            let part = $(elem).text();
            setTimeout(() => {
                let selectElem$ = $(viewSelector + " select").eq(i);
                if (selectElem$.length) {
                    selectElem$.val(part); //.change();
                    clientActions.showForm(null, selectElem$[0]);
                }
            }, 50);
        });
    });
};