#! /bin/bash
export SHELL=/bin/bash
env_path=/var/textplus/env_tgimport
if test -n "$1"; then
    env_path=$1 
fi

cp /var/textplus/readme.ipynb ~/readme.ipynb
# startdir=/var/textplus
startdir=~
if test -n "$JUPYTERHUB_API_TOKEN"; then
    cd $startdir && ${env_path}/bin/jupyterhub-singleuser --ip 0.0.0.0
else
  
    # JUPYTER_TOKEN=abcd
    JUPYTER_TOKEN=$("${env_path}/bin/python" -c "import random, string; print(''.join(random.SystemRandom().choice(string.ascii_lowercase + string.digits) for _ in range(32)))")
    echo JUPYTER_TOKEN: $JUPYTER_TOKEN
    cd $startdir && JUPYTER_TOKEN=$JUPYTER_TOKEN ${env_path}/bin/jupyter lab --ip 0.0.0.0
fi