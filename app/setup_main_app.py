import os
import glob
import shutil
import sys
import json
import re
import jupyter
import lib_actions
import lib_filters
import lib_presentation
import lib_util
import ipywidgets as widgets
from jinja2 import Environment, BaseLoader, FileSystemLoader, select_autoescape
from IPython.display import display, HTML


def setup_api_token():
    """This will copy the API token from the file 'api_token.tmp' to ~/.jn-secrets.
    If 'api_token.tmp' does not exist, it is created and needs to be edited.
    Then, the notebook needs to be run again."""

    api_token = ""
    tmpfile = "api_token.tmp"
    msg_instructions = re.sub(
        "\n +",
        "\n",
        f"""Copy the API token from the Jupyterhub Control Panel or, when running locally, 
    from the output of the Jupyter server in the terminal.
    Paste it in the file <a href="{tmpfile}">{tmpfile}</a> after "token=".
    Then run this cell again.""",
    )

    secrets_file = os.path.expanduser("~/.jn-secrets")
    api_token_from_secretsfile = lib_util.read_value_from_json_file(
        secrets_file, "api_token"
    )
    api_token_from_tmpfile = ""

    if "JUPYTERHUB_API_TOKEN" in os.environ:
        with open(tmpfile, "w") as f:
            f.write(f"token={os.environ['JUPYTERHUB_API_TOKEN']}")
    elif "JUPYTER_TOKEN" in os.environ:
        with open(tmpfile, "w") as f:
            f.write(f"token={os.environ['JUPYTER_TOKEN']}")
    if os.path.exists(tmpfile):
        with open(tmpfile) as f:
            for l in f.readlines():
                l = l.strip()
                if l.startswith("token="):
                    api_token_from_tmpfile = l[len("token=") :].strip()
                    break
    if api_token_from_tmpfile:
        api_token = api_token_from_tmpfile
        lib_util.update_values_in_json_file(secrets_file, {"api_token": api_token})
        # print("API token successfully stored.")
        os.remove(tmpfile)
    else:
        if not os.path.exists(tmpfile):
            with open(tmpfile, "w") as f:
                f.write('# Paste the API token in this file after "token=".\ntoken=')
        if api_token_from_secretsfile:
            api_token = api_token_from_secretsfile
            display(
                HTML(
                    "API token found. If you want to update the API token,"
                    + f' paste it in the file <a href="{tmpfile}">{tmpfile}</a> after "token=", then run this cell again.'
                )
            )
        else:
            display(HTML(msg_instructions))

    return api_token


def make_token_available_to_js(api_token):
    import os
    import glob
    import lib_presentation
    import lib_util
    from IPython.display import display, HTML

    for item in ("app/web/ext", "app/tmp"):
        if not os.path.exists(item):
            os.makedirs(item)
    startdir = os.path.abspath(".")

    html = lib_presentation.html_with_js_vars(
        {"api_token": api_token, "notebookdir_abs": startdir}
    )
    display(HTML(html))


def js_write_url_to_file(location_file):
    import time

    if os.path.exists(location_file):
        os.remove(location_file)
    html = lib_presentation.render_template_file("app/templates/loc_util.html", {})
    display(HTML(html))
    # the efect of the previous command is to run JavaScript code
    # that creates the file "location.json"
    loc_updated = False
    c = 0
    while not loc_updated and c < 50:
        c += 1
        time.sleep(0.1)
        if os.path.exists(location_file):
            loc_updated = True
    return loc_updated


def update_appdir(app_rootdir):
    # Update the contents of app/ using the contents of app_custom/
    srcs = sorted(glob.glob(f"{app_rootdir}/app_custom/**/*", recursive=True))
    for src in srcs:
        relpath = os.path.relpath(src, f"{app_rootdir}/app_custom")
        dest = f"{app_rootdir}/app/{relpath}"
        if os.path.exists(dest):
            os.remove(dest)
        lib_util.ensure_parentdir(dest)
        shutil.copy2(src, dest)


def get_root_dir(startdir, notebookdir_rel):
    import os

    startdir_parts = startdir.split(os.sep)
    notebookdir_rel_parts = notebookdir_rel.split("/")
    for i, part in enumerate(list(reversed(notebookdir_rel_parts))):
        if part != list(reversed(startdir_parts))[i]:
            return None
    return os.sep.join(startdir_parts[: -len(notebookdir_rel_parts)])


def setup_filters(jinja_env):
    for item in dir(lib_filters):
        obj = getattr(lib_filters, item)
        obj = getattr(sys.modules[__name__], item, obj)
        if callable(obj):
            jinja_env.filters[item] = obj
    return jinja_env


def setup_app_env(env_from_config, api_token, notebookdir_rel, root_url, root_dir, app_rootdir):
    loader = FileSystemLoader(f"{app_rootdir}/app/templates")
    jinja_env = Environment(loader=loader, autoescape=select_autoescape())
    jinja_env = setup_filters(jinja_env)

    # clear file that contains the request info:
    temp_in = "app/tmp/tmp_req.json"
    # temp_out = "tmp_resp.txt"
    with open(temp_in, "w") as f:
        f.write("{}")
    app_env = env_from_config
    app_env["tmp_req"] = temp_in
    app_env["jinja_env"] = jinja_env
    app_env["api_token"] = api_token
    app_env["notebookdir_rel"] = notebookdir_rel
    app_env["root_url"] = root_url
    app_env["root_dir"] = root_dir
    app_env["secrets_file"] = os.path.expanduser("~/.jn-secrets")

    tmpdir = "./app/tmp"
    logfile = f"{tmpdir}/log.txt"
    logfile_abs = os.path.abspath(logfile)
    app_env["tmpdir"] = tmpdir
    app_env["logfile"] = logfile
    app_env["logfile_abs"] = logfile_abs
    if os.path.exists(app_env["logfile"]):
        os.remove(app_env["logfile"])
    if not os.path.exists(app_env["tmpdir"]):
        os.makedirs(app_env["tmpdir"])
    app_env["app_state_file"] = "./app/tmp/state.json"
    forms_config_template = "./app/forms_config_template.json"
    app_env["forms_config_file"] = "./app/forms_config.json"
    app_env["envpath"] = jupyter.__file__.rsplit(f"{os.sep}lib{os.sep}", 1)[0]
    with open(forms_config_template) as f:
        template = Environment(loader=BaseLoader).from_string(f.read())
        forms_config_text = template.render(**app_env)
        with open(app_env["forms_config_file"], "w") as f2:
            f2.write(forms_config_text)
        app_env["forms_config"] = json.loads(forms_config_text)

    return app_env


def setup_actions():
    """A mapping of action names to functions in lib_actions and in the current module."""
    import sys

    actions = {}
    ctxt = sys.modules[__name__]
    # functions in the current module override functions from lib_actions
    for module in (lib_actions, sys.modules[__name__]):
        for item in dir(module):
            prefix = "action__"
            if item.startswith(prefix):
                actions[item[len(prefix) :]] = getattr(module, item)
    return actions


def respond_to_query(view, app_env, actions):
    with open(app_env["tmp_req"]) as f:
        query_info = json.load(f)
        query_info["view"] = view
        action = actions[query_info["action"]]
    return action(query_info, app_env)


def setup_views(actions, app_env):

    output_view1 = widgets.Output(layout={"border": "1px solid black"})
    output_view2 = widgets.Output(layout={"border": "1px solid black"})
    info_view = widgets.Output(layout={"border": "1px solid black"})

    @output_view1.capture(clear_output=True)
    def start_view(event):
        display(
            HTML(
                actions["startview"](
                    {"update_app_state": "./app/tmp/state.json"}, app_env
                )
            )
        )

    @output_view1.capture(clear_output=True)
    def respond_to_query_view1(event):
        html = respond_to_query("view1", app_env, actions)
        display(HTML(html))

    # @output_view2.capture(clear_output=True)
    # def respond_to_query_view2(event):
    #     html = respond_to_query("view2", app_env, actions)
    #     display(HTML(html))

    # @info_view.capture(clear_output=True)
    # def respond_to_query_infoview(event):
    #     text = respond_to_query("infoview", app_env, actions)
    #     display(str(text)[:200] + "[...]")

    # button_load_infoview = widgets.Button(description="Info", layout={"width": "300px"})
    button_load_view1 = widgets.Button(description="view1", layout={"width": "300px"})
    # button_load_view2 = widgets.Button(description="view2", layout={"width": "300px"})

    # button_load_infoview.on_click(respond_to_query_infoview)
    button_load_view1.on_click(respond_to_query_view1)
    # button_load_view2.on_click(respond_to_query_view2)

    # display the widgets (views and buttons)
    # output_view2.clear_output()
    display(output_view1)
    # display(info_view)
    # display(output_view2)
    start_view(None)

    display(button_load_view1)
    # display(button_load_view2)
    # display(button_load_infoview)


def main(configfile):
    if "APP_ROOTDIR" not in os.environ:
        print(
            "Environment variable APP_ROOTDIR not available. Please run the main notebook."
        )
    else:
        app_rootdir = os.environ["APP_ROOTDIR"]
        env_from_config = {}
        if os.path.exists(configfile):
            try:
                with open(configfile) as f:
                    env_from_config = json.load(f)
            except Exception as e:
                print(str(e))
        api_token = setup_api_token()
        make_token_available_to_js(api_token)
        location_file = f"{app_rootdir}/app/tmp/location.json"
        loc_updated = js_write_url_to_file(location_file)
        if not os.path.exists(location_file):
            sys.stderr.write(
                f"File {location_file} not found. Try clearing the outputs of other open notebooks first, then run again."
            )
        else:
            # update_appdir(app_rootdir)
            with open(location_file) as f:
                info = json.load(f)
                root_url = info["root_url"]
                notebookdir_rel = info["notebookdir_rel"]
                root_dir = get_root_dir(app_rootdir, notebookdir_rel)
                if not notebookdir_rel:
                    sys.stderr.write(
                        "Please start JupyterLab from a different directory."
                    )
                else:
                    app_env = setup_app_env(
                        env_from_config,
                        api_token,
                        notebookdir_rel,
                        root_url,
                        root_dir,
                        app_rootdir,
                    )
                    app_env.update(env_from_config)
                    actions = setup_actions()
                    setup_views(actions, app_env)
