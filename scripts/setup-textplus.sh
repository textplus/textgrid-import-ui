#! /bin/bash
env_path="/var/textplus/env_tgimport"
env_name=env_tgimport
targetdir="/var/textplus"
setup_script="/var/textplus/textgrid-import-ui/scripts/setup-tgimport-ui.sh"
if [ -f "$setup_script" ]; then
    $setup_script ${env_path} ${env_name} ${targetdir}
else
    /var/textplus/setup-tgimport-ui.sh ${env_path} ${env_name} ${targetdir} 
fi

