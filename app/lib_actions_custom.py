import subprocess
import json
import sys
import os
import shutil
import re
import jupyter
import glob
import lib_presentation
import lib_util

# logging.basicConfig(level=logging.DEBUG)
tmpdir = "./app/tmp"
logfile = f"{tmpdir}/log.txt"
logfile_abs = os.path.abspath(logfile)
if os.path.exists(logfile):
    os.remove(logfile)
if not os.path.exists(tmpdir):
    os.makedirs(tmpdir)
app_state_file = "./app/tmp/state.json"
# envpath = "/var/textplus/env_tgimport"
envpath = jupyter.__file__.rsplit(f"{os.sep}lib{os.sep}", 1)[0]

# tg_server = "https://test.textgridrep.org"
# tg_server_sid = "https://test.textgridlab.org/1.0/Shibboleth.sso/Login?target=/1.0/secure/TextGrid-WebAuth.php?authZinstance=test.textgridlab.org"
tg_server = "https://beta.textgridrep.org"
tg_server_sid = "https://textgridlab.org/1.0/Shibboleth.sso/Login?target=/1.0/secure/TextGrid-WebAuth.php?authZinstance=textgrid-esx2.gwdg.de"


temp_resultviews = {
    "1_create_configs": f"{tmpdir}/1_create_configs.html",
    "2_create_metadata_files": f"{tmpdir}/2_create_metadata_files.html",
}
forms_data = {}
forms_data["0_store_tg_session_id"] = [
    [
        {"type": "h4", "value": "Specify the project name and a TextGrid Session ID"},
        {
            "type": "input",
            "name": "project_name",
            "label": "Project name:",
            "value": "",
            "attrs": {"class": "required"},
        },
        {
            "type": "input",
            "name": "tg_session_id",
            "label": "TextGrid Session ID: ",
            "after_label": f'<span class="hint">(Get one <a class="font-weight-bold" target=”_blank” href="{tg_server_sid}">here</a> if needed)</span>',
            "value": "",
            "attrs": {"class": "required"},
        },
        {
            "type": "button",
            "value": "Store Session ID",
            "attrs": {
                "class": "action onclick",
                "data-action": "submitForm",
                "data-action-server": "store_tg_session_id",
                "data-view": "view1",
            },
        },
        {
            "type": "button",
            "value": "Last used values",
            "attrs": {
                "class": "action onclick",
                "data-action": "loadValuesFromFile",
                "data-file": "./app/tmp/state.json",
            },
        },
    ]
]

forms_data["1_create_configs"] = [
    [
        {"type": "h4", "value": "Create configuration files"},
        {
            "type": "input",
            "name": "corpus_dir.0",
            "label": "Path to the folder with TEI files:",
            "value": "",
            "attrs": {
                "class": "required action onclick",
                "data-action": "becomeTarget_addField",
            },
        },
        {
            "type": "input",
            "name": "workdir",
            "label": "Path to the working directory:",
            "value": "workdir",
            "attrs": {
                "class": "required action onclick",
                "data-action": "becomeTarget",
            },
        },
        {
            "type": "input",
            "name": "tg_project_id",
            "label": "Internal TextGrid project ID:",
            "after_label": "<br />(Leave empty to create a new project, or use a project ID listed below)",
            "value": "",
        },
        {
            "type": "button",
            "value": "Create configs",
            "attrs": {
                "class": "action onclick",
                "data-action": "submitForm",
                "data-action-server": "tgm_create_config",
                "data-view": "view1",
            },
        },
        {
            "type": "button",
            "value": "Last used values",
            "attrs": {
                "class": "action onclick",
                "data-action": "loadValuesFromFile",
                "data-file": "./app/tmp/state.json",
            },
        },
        {"type": "hidden", "name": "update_app_state", "value": "./app/tmp/state.json"},
    ],
    [],
]
forms_data["2_create_metadata_files"] = [
    [
        {"type": "h4", "value": "Create TextGrid metadata files"},
        {
            "type": "button",
            "value": "Create metadata files",
            "attrs": {
                "class": "action onclick",
                "data-action": "submitForm",
                "data-action-server": "tgm_create_metadata_files",
                "data-view": "view1",
            },
        },
    ]
]

forms_data["3_upload_to_textgrid"] = [
    [
        {"type": "h4", "value": "Upload the data to TextGrid"},
        # {
        #     "type": "number",
        #     "name": "upload_limit",
        #     "label": "Stop after uploading this number of items (use -1 to upload all):",
        #     "value": "3",
        #     "attrs": {"class": "number"},
        # },
        {
            "type": "button",
            "value": "Upload to TextGrid / Update",
            "attrs": {
                "class": "action onclick",
                "data-action": "submitForm",
                "data-action-server": "upload_to_textgrid",
                "data-view": "view1",
            },
        },
        {
            "type": "button",
            "value": "Back",
            "attrs": {
                "class": "action onclick",
                "data-action": "submitForm",
                "data-action-server": "goto_tgm_create_metadata_files",
                "data-view": "view1",
            },
        },
        {
            "type": "html",
            "value": f"""<div class="alert alert-light" role="alert">Open your <a href="{tg_server}/projects" target="_blank">your personal TextGrid area</a>
in a browser (log in, then go to "My TextGrid", select "Unpublished Data"). 
You can check the results there When finished, but also already during the upload</div>""",
        },
        {"type": "hidden", "name": "update_app_state", "value": "./app/tmp/state.json"},
    ]
]


def resolve_path(path, app_env):
    # if path.split("/")[0] in (".", "..", ""):
    #     return path
    # else:
    #     return f"{root_dir}/{path}"
    return os.path.abspath(path.format(**app_env))


def unflatten(val: dict) -> dict:
    try:
        # ensure only values serializable as json are accepted
        val = json.loads(json.dumps(val))
        if not isinstance(val, dict):
            return val
        temp_data = {"toplevel": {}}
        for path, v in val.items():
            path_parts = ["toplevel"] + path.split(".")
            for i, part in enumerate(path_parts):
                container_is_list = False
                if re.match(r"\d+$", part):
                    part = int(part)
                    container_is_list = True
                container_path = ".".join(path_parts[:i])
                if container_path not in temp_data:
                    temp_data[container_path] = {}
                    if container_is_list:
                        temp_data[container_path] = []
                if container_is_list:
                    temp_data[container_path].append(v)
                else:
                    temp_data[container_path][part] = v
        for k, v in sorted(temp_data.items(), reverse=True):
            path_parts = k.split(".")
            for i, part in enumerate(reversed(path_parts)):
                if i < len(path_parts) - 1:
                    path_container = ".".join(path_parts[: len(path_parts) - i - 1])
                    temp_data[path_container][part] = v
        return temp_data["toplevel"]
    except Exception as e:
        sys.stderr.write(str(e))
        return val


def extract_info(text, pattern):
    """Return a list of text parts that correspond to the first group in the given regex pattern"""
    results = []
    for line in text.splitlines():
        match_result = re.search(pattern, line)
        if match_result:
            results.append(match_result.group(1))
    return results


def action__startview(query_info={}, app_env={}):
    template = app_env["jinja_env"].get_template("fs.html")
    secrets = lib_util.load_json(app_env["secrets_file"])
    lib_util.update_values_in_json_file(app_state_file, secrets)

    html = template.render(
        view=query_info.get("view", "view1"),
        root_url=app_env["root_url"],
        notebookdir_rel=app_env["notebookdir_rel"],
        api_token=app_env["api_token"],
        # forms=forms_data["1_create_configs"],
        forms=forms_data["0_store_tg_session_id"],
    )
    return html


def action__store_tg_session_id(query_info={}, app_env={}):
    forms_config = app_env["forms_config"]
    lib_util.update_values_in_json_file(app_state_file, query_info)
    app_state = lib_util.load_json(app_state_file)
    tg_session_id = app_state["tg_session_id"]

    html_project_ids = ""
    server_part = f'--server {app_env["tglab_server"]}' if "tglab_server" in app_env else ""
    cmd = f'{envpath}/bin/tgadmin {server_part} --sid {tg_session_id} list'
    # cmd = [
    #     f"{envpath}/bin/tgadmin",
    #     "--server",
    #     app_env["tglab_server"],
    #     # "--test",
    #     "--sid",
    #     tg_session_id,
    #     "list",
    # ]
    out, err = lib_util.run_system_command(cmd, catch_errors=True)
    project_ids = [f"<div>{item}</div>" for item in (out or "---").split(os.linesep)]
    html_project_ids = f"""
<div class="font-weight-bold">Available TextGrid projects:</div>
{os.linesep.join(project_ids)}"""

    template = app_env["jinja_env"].get_template("fs.html")
    html = template.render(
        view=query_info.get("view", "view1"),
        root_url=app_env["root_url"],
        notebookdir_rel=app_env["notebookdir_rel"],
        api_token=app_env["api_token"],
        # forms=forms_data["1_create_configs"],
        action_messages=err,
        form=forms_config["Start"],
        form_path=[
            "Load workflow",
            "TextGrid Import",
            "TextGrid Import | Create configs",
        ],
        info_top=f'<h4>[{app_state.get("project_name", "PROJECT_NAME")}]</h4>',
        info_bottom=html_project_ids,
    )
    return html


def action__tgm_create_config(query_info={}, app_env={}):
    forms_config = app_env["forms_config"]
    query_info = unflatten(query_info)
    lib_util.update_values_in_json_file(app_state_file, query_info)
    app_state = lib_util.load_json(app_state_file)
    results = []
    problems = []
    workdir = resolve_path(query_info["workdir"], app_env)
    if not os.path.exists(workdir):
        os.makedirs(workdir)
    os.environ["workdir"] = workdir
    corpus_dirs = query_info["corpus_dir"]
    tg_project_id = query_info["tg_project_id"]
    project_name = app_state["project_name"]
    tg_session_id = app_state["tg_session_id"]

    if "tg_session_id" in query_info:
        lib_util.update_values_in_json_file(
            app_env["secrets_file"], {"tg_session_id": query_info["tg_session_id"]}
        )
    if not tg_project_id:
        server_part = f'--server {app_env["tglab_server"]}' if "tglab_server" in app_env else ""
        cmd = f'{envpath}/bin/tgadmin {server_part} --sid {tg_session_id} create "{project_name}"'
        # [
        #     f"{envpath}/bin/tgadmin",
        #     # "--test",
        #     "--server",
        #     app_env["tglab_server"],
        #     "--sid",
        #     tg_session_id,
        #     "create",
        #     project_name,
        # ],
        out, err = lib_util.run_system_command(cmd, catch_errors=True)
        if err:
            problems = [err]
        else:
            # read the response, which contains the textgrid project id and write it to a variable
            tg_project_id = re.search(r"TGPR-[\w-]+", out).group(0)
            app_state.update({"tg_project_id": tg_project_id})
            lib_util.update_values_in_json_file(app_state_file, app_state)

    if isinstance(corpus_dirs, str):
        corpus_dirs = [corpus_dirs]
    for corpus_dir in corpus_dirs:
        corpus_dir = resolve_path(corpus_dir, app_env)
        cmd_create_configs = f'cd "{workdir}" && {envpath}/bin/tg_configs -n "{project_name}" all -i "{corpus_dir}"'
        try:
            pipes = subprocess.Popen(
                cmd_create_configs,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                shell=True,
            )
            stdout, stderr = pipes.communicate()
            output = stdout.decode() + "\n" + stderr.decode()
            with open(logfile, "w", encoding="utf8") as f:
                f.write(output + "\n")
            # logging.debug(output1)
            results += extract_info(output, r".*INFO (.*\.yaml) initialized")
        except subprocess.CalledProcessError as e:
            problems.append(str(e))
    results = [
        f"""<div>{lib_presentation.link_to_file(
                os.path.relpath(item, app_env["root_dir"]), 
                app_env["root_url"]
            )}</div>"""
        for item in results
    ]
    results = (
        [
            f"""<div class="action-output">For full output see log file:<br />
{lib_presentation.link_to_file(os.path.relpath(logfile_abs, app_env["root_dir"]), app_env["root_url"])}
 (First ensure the file is not already open.)</div>
<div class="action-output">Created files:</div>"""
        ]
        + results
    )
    template = app_env["jinja_env"].get_template("fs.html")
    html = template.render(
        view=query_info.get("view", "view1"),
        root_url=app_env["root_url"],
        notebookdir_rel=app_env["notebookdir_rel"],
        api_token=app_env["api_token"],
        project_name=project_name,
        action_output_html="\n".join(results),
        action_messages="\n".join(problems),
        form_path=[
            "Load workflow",
            "TextGrid Import",
            "TextGrid Import | Create TextGrid metadata files",
        ],
        form=forms_config["Start"],
        info_top=f'<h4>[{app_state.get("project_name", "PROJECT_NAME")} ({app_state.get("tg_project_id", "###")})]</h4>',
    )
    lib_util.tofile(temp_resultviews["1_create_configs"], html, verbose=False)
    return html


def action__tgm_create_metadata_files(query_info={}, app_env={}):
    forms_config = app_env["forms_config"]
    lib_util.update_values_in_json_file(app_state_file, query_info)
    app_state = lib_util.load_json(app_state_file)
    app_state["result_dirs"] = []
    workdir = resolve_path(app_state["workdir"], app_env)
    if not os.path.exists(workdir):
        os.makedirs(workdir)
    project_name = app_state["project_name"]
    cmd_create_metadata_files = f'cd "{workdir}" &&  {envpath}/bin/tg_model -n "{project_name}" build-collection'
    output = ""
    problems = ""
    try:
        pipes = subprocess.Popen(
            cmd_create_metadata_files,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True,
        )
        stdout, stderr = pipes.communicate()
        output = stdout.decode() + "\n" + stderr.decode()
        with open(logfile, "w", encoding="utf8") as f:
            f.write(output + "\n")
    except subprocess.CalledProcessError as e:
        problems = str(e)
    action_output = extract_info(output, r".*INFO Project path:\s*(.*)")
    for i, item in enumerate(action_output):
        result_dir = os.path.relpath(item, app_env["root_dir"])
        action_output[i] = (
            f'Results: <span class="action onclick file fs-path" data-action="showInFileBrowser">{result_dir}</span><br />'
        )
        app_state["result_dirs"].append(result_dir)
    action_output += [
        f"""<div class="action-output">For full output see log file:<br />
{lib_presentation.link_to_file(os.path.relpath(logfile_abs, app_env["root_dir"]), app_env["root_url"])}
 (First ensure the file is not already open.)</div>"""
    ]
    lib_util.update_values_in_json_file(app_state_file, app_state)
    template = app_env["jinja_env"].get_template("fs.html")
    html = template.render(
        view=query_info.get("view", "view1"),
        root_url=app_env["root_url"],
        notebookdir_rel=app_env["notebookdir_rel"],
        api_token=app_env["api_token"],
        action_output_html="\n".join(action_output),
        action_messages=problems,
        form_path=[
            "Load workflow",
            "TextGrid Import",
            "TextGrid Import | Upload to TextGrid",
        ],
        form=forms_config["Start"],
        info_top=f'<h4>[{app_state.get("project_name", "PROJECT_NAME")} ({app_state.get("tg_project_id", "###")})]</h4>',
    )
    return html


def action__goto_tgm_create_metadata_files(query_info={}, app_env={}):
    with open(temp_resultviews["1_create_configs"], encoding="utf8") as f:
        return f.read()


def action__upload_to_textgrid(query_info={}, app_env={}):
    forms_config = app_env["forms_config"]
    app_state = lib_util.load_json(app_state_file)

    tg_project_id = app_state.get("tg_project_id", None)
    tg_session_id = app_state.get("tg_session_id", None)

    if not tg_session_id:
        return "Please provide a TextGrid session ID"

    if "update_app_state" in query_info:
        lib_util.update_values_in_json_file(
            query_info["update_app_state"], {"tg_project_id": tg_project_id}
        )

    for result_dir in app_state.get("result_dirs", []):
        result_dir_abs = f'{app_env["root_dir"]}/{result_dir}'
        tg_collection_files = glob.glob(
            f"{result_dir_abs}/**/*.collection", recursive=True
        )
        edition_files = sorted(
            glob.glob(
                f'{app_env["root_dir"]}/{result_dir}/**/*.edition', recursive=True
            )
        )
        if len(edition_files) > 10:
            print(
                "The upload will take some time, as each edition needs a few seconds to upload."
            )
        print(f"Processing {len(edition_files)} edition files...")
        for tg_collection_file in tg_collection_files:
            print(f"Collection file: {tg_collection_file}")
            if os.path.exists(f"{tg_collection_file}.imex"):
                server_part = f'--server {app_env["tglab_server"]}' if "tglab_server" in app_env else ""
                cmd = f'{envpath}/bin/tgadmin {server_part} --sid {tg_session_id} update-imex "{tg_collection_file}.imex"'
                # [
                #     f"{envpath}/bin/tgadmin",
                #     "--server",
                #     app_env["tglab_server"],
                #     "--sid",
                #     tg_session_id,
                #     "update-imex",
                #     f"{tg_collection_file}.imex",
                # ]
                out, err = lib_util.run_system_command(cmd)
            else:
                server_part = f'--server {app_env["tglab_server"]}' if "tglab_server" in app_env else ""
                cmd = f'{envpath}/bin/tgadmin {server_part} --sid {tg_session_id} put-aggregation {tg_project_id} "{tg_collection_file}"'
                # [
                #     f"{envpath}/bin/tgadmin",
                #     "--server",
                #     app_env["tglab_server"],
                #     "--sid",
                #     tg_session_id,
                #     "put-aggregation",
                #     tg_project_id,
                #     tg_collection_file,
                # ],
                out, err = lib_util.run_system_command(cmd, catch_errors=True)
            if out or err:
                with open(logfile, "w", encoding="utf8") as f:
                    f.write("\n\n".join((out, err, "")))

    template = app_env["jinja_env"].get_template("fs.html")
    html = template.render(
        view=query_info.get("view", "view1"),
        root_url=app_env["root_url"],
        notebookdir_rel=app_env["notebookdir_rel"],
        api_token=app_env["api_token"],
        action_messages=f"""<div class="action-output">For full output see log file:<br />
{lib_presentation.link_to_file(os.path.relpath(logfile_abs, app_env["root_dir"]), app_env["root_url"])}
 (First ensure the file is not already open.)</div>""",
        form_path=[
            "Load workflow",
            "TextGrid Import",
            "TextGrid Import | Upload to TextGrid",
        ],
        form=forms_config["Start"],
        info_top=f'<h4>[{app_state.get("project_name", "PROJECT_NAME")} ({app_state.get("tg_project_id", "###")})]</h4>',
    )
    return html


def action__update_metadata_in_textgrid(query_info={}, app_env={}):
    query_info.update({"only_update_metadata": True})
    return action__upload_to_textgrid(query_info, app_env)
