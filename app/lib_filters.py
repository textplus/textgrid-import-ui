def form_entry(entry):
    html = ""
    type_or_tag = entry[
        "type"
    ]  # input, textarea, checkbox, select, hidden, number, date, email, tel, ${HTML_tag}
    name = entry.get("name", "")
    label = entry.get("label", "")
    after_label = entry.get("after_label", "")
    value = entry["value"]
    extra_html = entry.get("extra_html", "")
    attrs = " ".join([f'{k}="{v}"' for k, v in entry.get("attrs", {}).items()])
    class_hidden = ""
    br = "<br />"
    if type_or_tag == "hidden":
        class_hidden = " hidden"
        html = (
            f'<input type="hidden" id="{name}" name="{name}" {attrs} value="{value}" />'
        )
    elif type_or_tag == "checkbox":
        checked = 'checked="checked"' if value else ""
        html = f"""<span class="label-etc"><input type="checkbox" id="{name}" name="{name}" {checked} {attrs}/>
<label for="{name}">{label}</label>{after_label}</span>"""
    elif type_or_tag == "select":
        options = [f'<option value="{item}">{item}</option>' for item in value]
        html = f"""<span class="label-etc"><label for="{name}">{label}</label>{after_label}</span> <select 
id="{name}" name="{name}" {attrs}>{options}</select>"""
    elif type_or_tag == "input":
        html = f"""<span class="label-etc"><label for="{name}">{label}</label>{after_label}</span><br /><input 
type="text" id="{name}" name="{name}" {attrs} value="{value}" />"""
    elif type_or_tag in ("number", "date", "email", "tel"):
        html = f"""<span class="label-etc"><label for="{name}">{label}</label>{after_label}</span><br /><input 
type="{type_or_tag}" id="{name}" name="{name}" {attrs} value="{value}" />"""
    elif type_or_tag == "textarea":
        html = f"""<span class="label-etc"><label for="{name}">{label}</label>{after_label}</span><br /><textarea 
id="{name}" name="{name}" {attrs}>{value}</textarea>"""
    elif type_or_tag == "html":
        html = value        
    else:
        html = f"<{type_or_tag} {attrs}>{value}</{type_or_tag}>"
        br = ""
    return f'<div class="form-entry{class_hidden}">{html + extra_html + br}</div>'
