# TextGrid Import UI


## Description

A Jupyter Notebook-based user interface for [TextGrid Import Modelling](https://gitlab.gwdg.de/textplus/textplus-io/textgrid-import-ui), which is a command line tool that facilitates the creation of the metadata files required for importing data into [TextGrid Rep](https://textgridrep.org/). The objective is to make the process of importing data into TextGrid Rep easy and user friendly.

## Installation

### On the Text+ Jupyterhub (jupyter-cloud.gwdg.de) 

1. Open and run the notebook *readme.ipynb* in the home directory. This will make textgrid-import-ui available there.

### On a local machine with Docker

1. Get textgrid-import-ui:
```bash
git clone https://oauth:UcX7kPHBvuG1j1z4WxTb@gitlab.gwdg.de/textplus/textplus-io/textgrid-import-ui.git
cd textgrid-import-ui
```

2. If necessary, download and install Docker.

3. On first use, build the Docker image:
```bash
docker image rm --force tg_ui && docker image build . -t tg_ui
```

4. Run a container from the image, then use docker logs to copy the API token:
```bash
docker run -d  -p 8888:8888 tg_ui
# The previous command will output the ID of the container. Use this in the following command:
docker logs <CONTAINER_ID>
# Copy the value of the Jupyter API token, which is shown in a line at the top (it starts with "JUPYTER_TOKEN: ")
```

5. Open a browser, go to localhost:8888/, paste the token in the input field labeled "Password or token:", log in.

6. Open and run the notebook *readme.ipynb* in the home directory. This will make textgrid-import-ui available there.


## Usage

Use the main notebook (*textgrid-import-ui/00-main-app.ipynb*). On first use, make sure to read the instructions in the notebook. Make sure that there is at least one folder with TEI files on the machine where Jupyterlab is running (in the cloud or on the local machine).

## Notes

- Refreshing files: after an opened file (other than a notebook) has changed, Jupyterlab does not usually refresh it automatically. To refresh, close it and open it again. Also consider this when clicking on links that open files. 