#!/bin/bash
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
mkdir -p ${SCRIPT_DIR}/web/ext
# download jquery
cd ${SCRIPT_DIR}/web/ext
file=jquery-3.7.1.min.js
if [ ! -f "$file" ]; then
    wget https://code.jquery.com/jquery-3.7.1.min.js 
fi
# download and unpack bootstrap
file=bootstrap-4.3.1-dist.zip
if [ ! -f "$file" ]; then
    wget https://github.com/twbs/bootstrap/releases/download/v4.3.1/bootstrap-4.3.1-dist.zip
    unzip $file
fi
